locals {
  node_volume_name = "${var.app_name}-${var.container_name}-node-cluster-ebs"
}

# Node without persistance - Run on Fargate
data "template_file" "node" {
  template = file("${path.module}/task-definitions/node.json")

  vars = {
    image          = "${var.registry_url}/${var.container_name}:${var.container_tag}"
    container_name = var.container_name
    log_group      = var.cloudwatch_log_group
    region         = data.aws_region.selected.name
  }
}

resource "aws_ecs_task_definition" "node" {
  count                    = var.persist_node_data ? 0 : 1
  container_definitions    = data.template_file.node.rendered
  cpu                      = var.desired_task_cpu
  execution_role_arn       = data.aws_iam_role.selected.arn
  family                   = "${var.app_name}-${var.container_name}-node"
  memory                   = var.desired_task_memory
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  task_role_arn            = data.aws_iam_role.selected.arn
  tags = {
    Product = var.app_name
  }
}

# Node with persistance - Run on EC2
data "template_file" "node_persist" {
  template = file("${path.module}/task-definitions/node_with_persistance.json")

  vars = {
    image          = "${var.registry_url}/${var.container_name}:${var.container_tag}"
    container_name = var.container_name
    volume_name    = local.volume_name
    log_group      = var.cloudwatch_log_group
    region         = data.aws_region.selected.name
  }
}

resource "aws_ecs_task_definition" "node_persist" {
  count                    = var.persist_node_data ? 1 : 0
  container_definitions    = data.template_file.node_persist.rendered
  cpu                      = var.desired_task_cpu
  execution_role_arn       = data.aws_iam_role.selected.arn
  family                   = "${var.app_name}-${var.container_name}-node"
  memory                   = var.desired_task_memory
  network_mode             = "awsvpc"
  requires_compatibilities = ["EC2"]
  task_role_arn            = data.aws_iam_role.selected.arn

  volume {
    name = local.volume_name
    docker_volume_configuration {
      scope         = "shared"
      autoprovision = true
      driver        = "rexray/ebs"
      driver_opts = {
        "volumetype" = "gp2"
        "size"       = "100"
      }
      labels = {}
    }
  }

  tags = {
    Product = var.app_name
  }
}
