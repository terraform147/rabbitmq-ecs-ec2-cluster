# RabbitMQ Cluster on AWS ECS with Capacity Provider and Fargate

## Requirements

To setup the RabbitMQ Cluster you need to have the following infrastructure:

- Network Infrastructure like VPC, Subnets, etc. [Base Network Infra](https://gitlab.com/terraform147/base-network-infra)
- External and Internal [Load Balancer](https://gitlab.com/terraform147/load-balancer)
- ECS Cluster with [Capacity Provider](https://gitlab.com/terraform147/ecs-cluster-capacity-provider)
- An HTTPS Certificate allocated on your External Load Balancer on 443 port. (If you want to use HTTPS)
- [Prometheus](https://gitlab.com/terraform147/prometheus) 

## Import 
```
module "rabbitmq-cluster" {
    app_name = ""
    cloudwatch_log_group = ""
    container_name = ""
    container_tag = ""
    domain = ""
    registry_url = ""
    setup_prometheus = false
    setup_dns = false
    persist_node_data = false
}
```

### Load Balancing

 You will probably need to access the RabbitMQ management, you can do that by 
 accessing directly by the url of the  external load balancer, or setup a DNS 
 if you want to. 
 On the `load_balancer.tf` file you will find both options  If you are going to 
 use HTTPS you need to edit the `load_balancer.tf` file  and comment the 
 `resource "aws_lb_listener_rule" "http"` and discomment the
 `resource "aws_lb_listener_rule" "https"`.
 Also remember to set on your `terraform.tfvars` the variable `setup_dns` to true.

 ### Prometheus

 If you don't have [Prometheus](https://prometheus.io) set up, you need
 to comment its configurations on `security_group.tf` and on `load_balancer.tf`
 If you need to have Prometheus discomment its section on `service.tf`
 Also remember to set on your `terraform.tfvars` the variable `setup_prometheus`
 to true.

 # Persisting or not RabbitMQ data

What I can say about this today(mar/2020): You can persist the data. But you can't restore properly.

Why is that: RabbitMQ uses the following configuration to setup the node `rabbit@${hostname}`.

The `hostname` need to be a fixed value so you can save and restore the database properly. 
The simplest way is to using an A record, and via DNS resolving the alias to the ip of the machine.
So far I was not able to come up with a satisfatory configuration of RabbitMQ 
using DNS to use the alias instead of the ip of host for the `master` node at least. 

The `master` node is configured to run on EC2 with an EBS volume, where does it 
store its data, and open the port to the Management console of RabbitMQ, and does
the setup using the Internal/External load balancers.

The `nodes` are configured to run on EC2 or Fargate, you can choose one based on
the `persist_node_data` value. 

Both `master` and `nodes` register themselves in the same Service Discovery of AWS CloudMap. So all the ip addresses
will be under the same DNS registry.

The above setup of networking uses [DNS Peer Discovery](https://www.rabbitmq.com/cluster-formation.html#configuring) of RabbitMQ.

You can have a cluster with autoscaling, persisting data, but you are not able to restore the data. =/
