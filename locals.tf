locals {
  url          = "rabbit.${var.domain}"
  internal_url = "rabbit.internal.${var.domain}"
}
