resource "aws_ecs_service" "node" {
  cluster         = data.aws_ecs_cluster.selected.id
  desired_count   = var.min_tasks
  launch_type     = "EC2"
  name            = "${var.container_name}-nodes"
  task_definition = var.persist_node_data ? aws_ecs_task_definition.node_persist[0].arn : aws_ecs_task_definition.node[0].arn

  deployment_controller {
    type = "ECS"
  }

  lifecycle {
    ignore_changes = [desired_count]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.rabbit.arn
    container_name   = "rabbitmq"
    container_port   = 5672
  }

  service_registries {
    registry_arn = aws_service_discovery_service.this.arn
  }

  network_configuration {
    assign_public_ip = false
    security_groups  = [aws_security_group.this.id]
    subnets          = tolist(data.aws_subnet_ids.private.ids)
  }

  depends_on = [aws_lb_target_group.rabbit]
}
